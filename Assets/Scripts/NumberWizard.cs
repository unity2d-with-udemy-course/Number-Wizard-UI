﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.PlayerLoop;

public class NumberWizard : MonoBehaviour
{
    [SerializeField] int min;
    [SerializeField] int max;
    [SerializeField] TextMeshProUGUI guessText;
    private int guess;

    // Start is called before the first frame update
    void Start()
    {
        Next();
    }

    public void OnPressHigher()
    {
        min = guess + 1;
        if (min > max) min = max;
        Next();
    }
    
    public void OnPressLower()
    {
        max = guess - 1;
        if (min > max) max = min;
        Next();
    }

    void Next()
    {
        //guess = (min + max)/2;
        guess = Random.Range(min, max + 1);
        guessText.SetText(guess.ToString());
        //guessText.text = guess.ToString(); (c'est pareil)
    }
    
}
